# collection of ideas

## Links

https://www.w3schools.com/w3css/default.asp

https://codepen.io/amit_sheen/pen/QWGjRKR

https://tailwindcss.com https://github.com/tailwindlabs/tailwindcss

https://projects.lukehaas.me/css-loaders/

https://www.w3schools.com/howto/howto_css_loader.asp

https://bootswatch.com/pulse/

## Basic configuration

- [x] Margin
- [x] Padding
- [x] Font-Family
- [x] Font-Size
- [x] Font-Weight

- [ ] Variables

* [x] Colors
  - [x] Primary
  - [x] Secondary
  - [x] Success
  - [x] Warning
  - [x] Danger
  - [x] Dark
  - [x] Light

- [ ] Size
- [ ] Font-Family primary/secondary

- [x] Title
- [x] List
  - Problem: **ol würi schu nummeriara**
- [x] Link
- [x] Paragraph

## Components

- [x] (normal) Container (different Sizes)
- [x] Flex Container (basic config)
- [ ] Grid Container (basic config)
- [ ] Flex-Item?
- [ ] Grid-Item

- [x] Button
  - Problem: :active effect by flat and disabled
- [ ] Button Block
- [ ] Button-Groups
- [x] BG-Color

- [ ] Left
- [ ] Right
- [ ] Center (x-axis)
- [ ] Top
- [ ] Buttom
- [ ] Center (y-axis) (Combinations of the above)

- [x] Linear Progress Bar
- [x] Circle Progress

- [ ] Navbar (Hamburger)
- [ ] Tables https://css-tricks.com/responsive-data-tables/
- [ ] Forms

  - [ ] Legends
  - [ ] Insput fields
  - [ ] Checkbox
  - [ ] Radio buttons
  - [ ] Slider
  - [ ] Validation
  - [ ] Disable
  - [ ] File uplaod

- [ ] Alerts
- [ ] Badges
- [ ] List groups
- [ ] Cards
- [ ] Dialogs

## Structure

```
.
|- css
|   \- main.css
|
|- scss
    \- config
    |   \- _font.scss
    |   |- _tags.scss
    |
    |- _utilities
    |   \- _background.scss
    |   |- _blockquotes.scss [todo ?]
    |   |- _bottons.scss
    |   |- _colons.scss [todo]
    |   |- _container.scss [extend] (cards, list-groups)
    |   |- _forms.scss [todo] (legend, inputfield, radio buttons, checkboxes, sliders)
    |   |- _indicators.scss [todo] (Alerts, Badges)
    |   |- _margin.scss
    |   |- _modals.scss [todo] (toasts)
    |   |- _navbars.scss [todo]
    |   |- _navs.scss [todo] (tabs, pills, breadcrumvs, pagination)
    |   |- _padding.scss
    |   |- _progress.scss
    |   |- _spinner.scss
    |   |- _tables.scss [todo]
    |   |- _text.scss
    |
    |- variables
    |   \- _color.scss
    |   |- _font.scss
    |
    |- _config.scss
    |- _functions.scss
    |- _utilities.scss
    |- _variables.scss
    |- style.scss [Main file]
```
